package com.yeeld.api.katharthis;


import com.yeeld.api.dao.UserRepository;
import com.yeeld.api.model.User;
import io.katharsis.queryspec.QuerySpec;
import io.katharsis.repository.ResourceRepositoryBase;
import io.katharsis.repository.ResourceRepositoryV2;
import io.katharsis.resource.list.ResourceList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by minh.le on 11/17/17.
 */

public class UserResourceRepository extends ResourceRepositoryBase<User, Long> {

    @Autowired
    private UserRepository userRepository;

    public UserResourceRepository () {
        super(User.class);
    }

    @Override
    public synchronized void delete(Long id) {
        userRepository.delete(id);
    }

    @Override
    public synchronized <S extends User> S save(S user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public synchronized ResourceList<User> findAll(QuerySpec querySpec) {
        return querySpec.apply(userRepository.findAll());
    }
}
