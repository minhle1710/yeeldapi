package com.yeeld.api.dao;

import com.yeeld.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Created by minh.le on 11/17/17.
 */
@Component
public interface UserRepository extends JpaRepository<User, Long> {
}
