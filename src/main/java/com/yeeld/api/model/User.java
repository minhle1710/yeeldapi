package com.yeeld.api.model;

import io.katharsis.resource.annotations.JsonApiId;
import io.katharsis.resource.annotations.JsonApiResource;

import javax.persistence.*;

/**
 * Created by minh.le on 11/17/17.
 */

@Entity
@JsonApiResource(type = "users")
public class User {

    @JsonApiId
    @Id
    private Long id;

    private String name;

    private String email;

    public User () {

    }

    public User (Long id, String name, String email) {

        this.id = id ;
        this.name = name ;
        this.email = email;

    }
}


