package com.yeeld.api;

/**
 * Created by minh.le on 11/17/17.
 */
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.katharsis.spring.jpa.SpringTransactionRunner;

@Configuration
public class JpaConfig {

    @Bean
    public SpringTransactionRunner transactionRunner() {
        return new SpringTransactionRunner();
    }
}